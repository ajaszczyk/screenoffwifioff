package com.ajas.screenoffwifioff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class PowerReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
		boolean actionOff = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_POWER_OFF, false);
		boolean actionOn = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_POWER_ON, false);
		
		if(WifiActivity.development) Utils.log2File("Power off " + actionOff + " on " + actionOn);
		
		if(actionOff || actionOn) {
			String action = intent.getAction();
			if(WifiActivity.development) Utils.log2File("Power action: "+action);
			
			if(actionOn && action.equals(Intent.ACTION_POWER_CONNECTED)) {
		        Utils.startWifi(context);
		    }
		    
			if(actionOff && action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
		        Utils.stopWifi(context);
		    }
		}
	}

}
