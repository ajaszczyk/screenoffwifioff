package com.ajas.screenoffwifioff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;


public class MobileDataReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(WifiActivity.development) Utils.log2File("Received action: " + intent.getAction());
		
		int extraDataState = intent.getIntExtra(TelephonyManager.EXTRA_STATE, TelephonyManager.DATA_DISCONNECTED);
		SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
		boolean wifiToogleAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_TOOGLE, false);
		wifiToogleAction = false;
		
		switch (extraDataState) {
		//case TelephonyManager.DATA_CONNECTING:;
		case TelephonyManager.DATA_CONNECTED:
			if(wifiToogleAction) {
		    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.WIFI_ON))
		    		 EventsQueue.getInstance().remove(Event.WIFI_ON);
		    	 //Utils.stopWifi(context);
		    	 
		    	 /*if(WifiActivity.development) Utils.log2File("preferences action wifi "+wifiToogleAction);
		    	 if(wifiToogleAction) {
		    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF);    		 
		    		 if(null == delay || "0".equals(delay))
		    			 Utils.stopWifi(context);
		    		 else {
		    			 long delayVal = Long.parseLong(delay);
		    			 EventsQueue.getInstance().add(new Event("Wifi off", delayVal * 1000, Event.WIFI_OFF));
		    		 }
		    		 //System.out.println("Wifi off added, delay: "+ delay);
		    	 }*/
	    	  }

			break;

		case TelephonyManager.DATA_DISCONNECTED:
			if(wifiToogleAction) {
		    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.WIFI_OFF))
		    		 EventsQueue.getInstance().remove(Event.WIFI_OFF);
		    	 //Utils.startWifi(context);
		    	 
		    	 /*if(WifiActivity.development) Utils.log2File("preferences action wifi"+wifiToogleAction);
		    	 if(wifiToogleAction) {
		    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON);
		    		 if(null == delay || "0".equals(delay))
		    			 Utils.startWifi(context);
		    		 else {
		    			 long delayVal = Long.parseLong(delay);
		    			 EventsQueue.getInstance().add(new Event("Wifi on", delayVal * 1000, Event.WIFI_ON));
		    		 }
		    		 //System.out.println("Wifi on added, delay: "+ delay);
		    	 }*/
	    	  }

			break;


		/*case TelephonyManager.DATA_SUSPENDED:
			//data_toggle.setChecked(true);
			break;*/
		}
	}

}
