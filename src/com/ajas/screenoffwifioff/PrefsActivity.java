package com.ajas.screenoffwifioff;

import android.app.AlarmManager;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class PrefsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
    private CheckBoxPreference keepFgCB;
    private ListPreference intervalPreference;
    
    private AlarmManager alarmManager;
    private SharedPreferences sharedPreferences;
	
    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
		
		keepFgCB = (CheckBoxPreference) findPreference(Constans.PREFERENCES_KEEP_FOREGROUND);
		keepFgCB.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				if (Constans.PREFERENCES_KEEP_FOREGROUND.equals(arg0.getKey())) {
					if(arg1 instanceof Boolean){
						if (Boolean.FALSE.equals(((Boolean)arg1))) {
							Utils.removePreference(sharedPreferences, Constans.PREFERENCES_KEEP_FOREGROUND);
							if(null != ScreenStateService.getInstance())
								ScreenStateService.getInstance().stopFg();
						} else {
							if(null != ScreenStateService.getInstance())
								ScreenStateService.getInstance().startFg();
						}
					}
				}
				return true;
			}
		});
    }
    
    @Override
	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		//setOptionsSummary();
		this.onContentChanged();
	}
}
