package com.ajas.screenoffwifioff;

import java.util.ArrayList;

public class EventsQueue {
	private static EventsQueue instance = new EventsQueue();
	private static ArrayList<Event> queue = new ArrayList<Event>();
	
	public static EventsQueue getInstance(){
		return instance;
	}
	
	public synchronized Event get() {
		if(null == queue || queue.size() == 0) return null;
		Event result = null;
		
		Long now = System.currentTimeMillis();
		for(Event event: queue) {
			if(now.compareTo(event.getTime()) >= 0) {
				result = event;
				break;
			}
		}
		if(null != result) queue.remove(result);
		
		return result;
	}
	
	public synchronized boolean add(Event event) {
		if(!hasEventOfSameType(event)) {
			queue.add(event);
			return true;
		}
		return false;
	}
	
	public synchronized void clear() {
		queue.clear();
	}
	
	public synchronized boolean hasEventOfSameType(Event event) {
		return hasEventOfSameType(event.getKind());
	}
	
	public synchronized boolean hasEventOfSameType(String kind) {
		for(Event e: queue) {
			if(kind.equals(e.getKind())) return true; 
		}
		return false;
	}
	
	public synchronized void remove(String kind) {
		ArrayList<Event> removeList = new ArrayList<Event>();
		
		for(Event e: queue) {
			if(kind.equals(e.getKind()))
				removeList.add(e); 
		}
		
		for (Event er: removeList)
			queue.remove(er);
		
		removeList.clear();
	}
}
