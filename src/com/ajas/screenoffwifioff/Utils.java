package com.ajas.screenoffwifioff;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Locale;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

public class Utils {
	
	public static SharedPreferences getSharedPreferences(Context context) {
		return context.getSharedPreferences(Constans.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
	}
	
	public static SharedPreferences getDefaultSharedPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public static String getPreferenceValue(SharedPreferences sharedPreferences, String preference) {
		return sharedPreferences.getString(preference, null);
	}
	
	public static void setPreferenceValue(SharedPreferences sharedPreferences, String preference, String value) {
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	editor.putString(preference, value);
    	editor.commit();
	}
	
	public static boolean getPreferenceBooleanValue(SharedPreferences sharedPreferences, String preference, boolean defaultValue) {
		return sharedPreferences.getBoolean(preference, defaultValue);
	}
	
	public static void setPreferenceBooleanValue(SharedPreferences sharedPreferences, String preference, boolean value) {
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	editor.putBoolean(preference, value);
    	editor.commit();
	}
	
	public static void removePreference(SharedPreferences sharedPreferences, String preference) {
		SharedPreferences.Editor editor = sharedPreferences.edit();
    	editor.remove(preference);
    	editor.commit();
	}
	
	public static int getArrayPosition(String item, String[] array) {
		if(StringUtils.isEmpty(item) || null == array || array.length<1) return -1;
		
		int pos=0;
		for(String str:array){
			if(item.equals(str)) return pos;
			pos++;
		}
		return -1;
	}
	
	public static boolean isWifiEnabled (Context context) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wifiManager.isWifiEnabled();
	}
	
	public static boolean isWifiConnected (Context context) {
		//if(isWifiEnabled(context)) {
		ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return mWifi.isConnected();
		//} else return false;
	}
	
	public static int getWifiSignalStrength(Context context){
	    int MIN_RSSI        = -100;
	    int MAX_RSSI        = -55;  
	    int levels          = 101;
	    WifiManager wifi    = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);            
	    WifiInfo info       = wifi.getConnectionInfo(); 
	    int rssi            = info.getRssi();

	    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH){
	        return WifiManager.calculateSignalLevel(info.getRssi(), levels);
	    } else {             
	        // this is the code since 4.0.1
	        if (rssi <= MIN_RSSI) {
	            return 0;
	        } else if (rssi >= MAX_RSSI) {
	            return levels - 1;
	        } else {
	            float inputRange = (MAX_RSSI - MIN_RSSI);
	            float outputRange = (levels - 1);
	            return (int)((float)(rssi - MIN_RSSI) * outputRange / inputRange);
	        }
	    }
	}
	
	public static void stopWifi(final Context context) {
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	boolean dont = Utils.getPreferenceBooleanValue(Utils.getDefaultSharedPreferences(context), Constans.PREFERENCES_WIFI_POWER_OFF_EXCEPT, false);
	        	WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    		boolean wifiEnabled = wifiManager.isWifiEnabled();
	    		if(wifiEnabled && !dont) wifiManager.setWifiEnabled(false);
	        }
	    }).start();
	}
	
	public static void startWifi(final Context context) {
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    		  boolean wifiEnabled = wifiManager.isWifiEnabled();
	    		  if(!wifiEnabled) wifiManager.setWifiEnabled(true);
	        }
	    }).start();
	}
	
	public static void stopData(final Context context) {
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	setMobileDataEnabled(context, false);
	        }
	    }).start();
	}
	
	public static void startData(final Context context) {
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	setMobileDataEnabled(context, true);
	        }
	    }).start();
	}
	
	// > FROYO_SDK_VERSION;
	public static void setMobileDataEnabled(final Context context, final boolean enabled) {
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	        	final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	            Class conmanClass = null;
				try {
					conmanClass = Class.forName(conman.getClass().getName());
					final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
		            iConnectivityManagerField.setAccessible(true);
		            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
		            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
		            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
		            //final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", boolean.class);//getMethod(iConnectivityManagerClass, "setMobileDataEnabled");
		            setMobileDataEnabledMethod.setAccessible(true);
		            Object invoke = setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);

		            //internalSetMobileDataEnabled(conman, conmanClass, enabled);
				/*} catch (ClassNotFoundException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
					//logMethods(conman);
					//internalSetMobileDataEnabled(conman, conmanClass, enabled);
					//if(enabled)conman.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
		            //else conman.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
				} catch (NoSuchFieldException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
					//logMethods(conman);
					//internalSetMobileDataEnabled(conman, conmanClass, enabled);
					//if(enabled)conman.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
		            //else conman.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
				} catch (IllegalArgumentException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
					//if(enabled)conman.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
		            //else conman.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
				} catch (IllegalAccessException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
					//if(enabled)conman.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
		            //else conman.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
				} catch (NoSuchMethodException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
					//logMethods(conman);
					internalSetMobileDataEnabled(conman, conmanClass, enabled);
				} catch (InvocationTargetException e) {
					Log.e(MainActivity.class.getName(), e.getMessage(), e);
					Utils.log2File(e);
				*/} catch (Exception e) {
					if(!(e instanceof NoSuchMethodException)) {
						Log.e(MainActivity.class.getName(), e.getMessage(), e);
						Utils.log2File(e);
					}
					
					try {
						Class[] cArg = new Class[2];
						cArg[0] = String.class;
						cArg[1] = Boolean.TYPE;
						final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
						iConnectivityManagerField.setAccessible(true);
			            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
			            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
			            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", cArg);

						Object[] pArg = new Object[2];
						pArg[0] = context.getPackageName();
						pArg[1] = enabled;
						setMobileDataEnabledMethod.setAccessible(true);
						setMobileDataEnabledMethod.invoke(iConnectivityManager, pArg);
					} catch (Exception e1) {
						Log.e(MainActivity.class.getName(), e1.getMessage(), e1);
						Utils.log2File(e1);
					}
					
					//if(enabled)conman.startUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
		            //else conman.stopUsingNetworkFeature(ConnectivityManager.TYPE_MOBILE, "android.net.conn.CONNECTIVITY_CHANGE");
				}
	        }
	    }).start();
	}
	
	private static void internalSetMobileDataEnabled(Class iConnectivityManagerClass, Class conmanClass, boolean enabled) {
		Method setMobileDataEnabledMethod = getMethod(iConnectivityManagerClass, "setMobileDataEnabled");
		setMobileDataEnabledMethod.setAccessible(true);
		try {
			/*iConnectivityManagerField = conmanClass.getDeclaredField("mService");
			Object iConnectivityManager = iConnectivityManagerField.get(conman);
            iConnectivityManagerField.setAccessible(true);*/
			
			final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
         // final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            //final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
			
            //Object invoke = setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
		} catch (Exception e1) {
			Log.e(MainActivity.class.getName(), e1.getMessage(), e1);
			Utils.log2File(e1);
		}
	}
		
	private static void logMethods (ConnectivityManager cm) {
		log2File("ConnectivityManager methods");
		Method[] methods = cm.getClass().getMethods();
		for (Method method : methods) {
		   log2File("Method name: " + method.getName());
		   if (method.getName().equals("setMobileDataEnabled")) {
			   log2File("Method params: " + getParameterNames(method));
		   }
		}
	}
	
	private static Method getMethod (Class iConnectivityManagerClass, String name) {
		//log2File("ConnectivityManager methods");
		Method[] methods = iConnectivityManagerClass.getMethods();
		for (Method method : methods) {
		   //log2File("Method name: " + method.getName());
		   if (method.getName().equals(name)){ //"setMobileDataEnabled")) {
			   return method;
			   //log2File("Method params: " + getParameterNames(method));
		   }
		}
		return null;
	}
	
	public static String getParameterNames(Method method) {
		Class[] parameterTypes = method.getParameterTypes();
        //Parameter[] parameters = method.getParameters();
        String parameters = "";

        for (Class parameter : parameterTypes) {
            parameters += parameter.getSimpleName()+ ", ";
        }

        return parameters;
    }
		
	public static boolean fileExists(String fileName) {
		if(StringUtils.isEmpty(fileName)) return false;
		File file = new File(fileName);
		return file.exists();
	}
	
	public static boolean createDirectory(String name) {
		try {
			File file = new File(name.trim());
			return file.mkdir();
		} catch (Exception e) {}
		return false;
	}
	
	private static String getWorkingDirectory() {
		StringBuilder builder = new StringBuilder();
		//if(Environment.getExternalStorageDirectory().getTotalSpace()>0 /*&& Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())*/)
			builder.append(Environment.getExternalStorageDirectory().getAbsolutePath());
		//else 
		builder.append(File.separator);
		builder.append(Constans.WORKING_DIRECTORY);
		builder.append(File.separator);
		File outputFileDir = new File(builder.toString());
        outputFileDir.mkdirs();
        return builder.toString();
	}
	
	public static void showToast(String text, boolean isLong, Context context) {
		int duration = Toast.LENGTH_LONG;
		if(!isLong) duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(context, text, duration);
    	toast.show();
   }
	
	public static void log2File(String message) {
		
		if(StringUtils.isEmpty(message)) return ;
		
		if(!fileExists(getLogFileName())) {
			getWorkingDirectory();
			File outputFileDir = new File(getLogFileName());	        
	        try {
				outputFileDir.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
			
		
		FileWriter output = null;
		try {
			output = new FileWriter(getLogFileName(), true);
			output.write(new Date().toString());
			output.write(": ");
			output.write(message);
			output.write("\n\r");			
		} catch (IOException e) {
			Log.e(Constans.TAG, message, e); 
		} finally {
			try {
				if(null != output) output.close();
			} catch (IOException e) {
				Log.e(Constans.TAG, message, e); 
			}
		}
	}
	
	public static void log2File(Exception e) {
		if(null == e) return ;
		
		FileWriter output = null;
		PrintWriter printWriter = null;
		try {
			output = new FileWriter(getLogFileName(), true);
			output.write(new Date().toString());
			output.write(": ");
			printWriter = new PrintWriter (output);
			e.printStackTrace(printWriter);
			output.write("\n\r");
		} catch (IOException ioe) {
			Log.e(Constans.TAG, ioe.getMessage(), e); 
		} finally {
			if(null != printWriter) printWriter.close();
		}
	}
	
	public static void log2File(String message, Throwable e) {
		if(null == e) return ;
		
		FileWriter output = null;
		PrintWriter printWriter = null;
		try {
			output = new FileWriter(getLogFileName(), true);
			output.write(new Date().toString());
			output.write(": "+message);
			output.write("\n\r");
			printWriter = new PrintWriter (output);
			e.printStackTrace(printWriter);
			output.write("\n\r");
		} catch (IOException ioe) {
			Log.e(Constans.TAG, ioe.getMessage(), e); 
		} finally {
			if(null != printWriter) printWriter.close();
		}
	}
	
	public static String getLogFileName() {
		StringBuilder fileName = new StringBuilder();
        fileName.append(Environment.getExternalStorageDirectory());
        fileName.append(File.separator);
        if(Utils.getSdkVersion() >= Build.VERSION_CODES.KITKAT)
        	fileName.append(Constans.WORKING_DIRECTORY_KITKAT);
        else fileName.append(Constans.WORKING_DIRECTORY);
        fileName.append(File.separator);
        fileName.append(Constans.LOG_FILE);
        
		File file = new File(fileName.toString());
		if(!file.exists()) {
			new File(file.getParent()).mkdirs(); 
			try {
				file.createNewFile();
			} catch (IOException e) {
				Log.e(Constans.TAG, e.getMessage(), e); 
			}
		}
		
		return fileName.toString();
	}
	
	public static int getSdkVersion() {
		return Integer.valueOf(Build.VERSION.SDK).intValue();
	}
	
	public static boolean isLocalized() {
		Locale locale =	Locale.getDefault();
		String localStr = locale.toString();
		/*System.out.println(Locale.getDefault().getDisplayLanguage());
		System.out.println(Locale.getDefault().getLanguage());
		System.out.println(getResources().getConfiguration().locale);*/
		boolean localized = true;
		if(!"en".equalsIgnoreCase(locale.getLanguage())) {			
			localized = "zh_TW".equals(localStr) || localStr.startsWith("fr") || localStr.startsWith("de")
					|| localStr.startsWith("hu") || localStr.startsWith("it") || localStr.startsWith("pl")
					|| localStr.startsWith("pt") || localStr.startsWith("ru") || localStr.startsWith("es")
					|| localStr.startsWith("da") || localStr.contains("unknown");			

			//System.out.println("localized: "+localized+" "+localStr);
		}
		return localized;		
	}
	
	public static Notification createForegroundNotification(Context context, int icon, String title, String message, int id) {
		//log2File("showNotification "+title+" "+message);
		NotificationManager notificationManager = (NotificationManager)	context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		Notification note=new Notification(icon, message, System.currentTimeMillis());
		Intent i = new Intent(context, MainActivity.class);
        i.putExtra("NotifID", id);  
        PendingIntent detailsIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, i, 0);
		note.setLatestEventInfo(context, title, message, detailsIntent);
        note.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Notification.FLAG_FOREGROUND_SERVICE;
	    //PendingIntent i=PendingIntent.getActivity(context, 0,new Intent(context, NotificationMessage.class),0);
	    //note.setLatestEventInfo(context, title, message, PendingIntent.getActivity(/*MainActivity.getInstance()*/context.getApplicationContext(), 0, new Intent(), 0));
	    //note.number += 1;
	    //note.vibrate=new long[] {500L, 200L, 200L, 500L};
	    //note.flags=Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Notification.FLAG_FOREGROUND_SERVICE;	    
	    
	    //notificationManager.notify(0, note);
	    
	    return note;
	}
	
	public static String getDeviceInfo() {
		String s="Device-infos:";
		s += "\n OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")";
		s += "\n OS API Level: " + android.os.Build.VERSION.SDK;
		s += "\n Device: " + android.os.Build.DEVICE;
		s += "\n Model (and Product): " + android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")";
		return s;
	}
	
	public static boolean isOnline(Context context) {
        try {
        	ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isAvailable() &&  networkInfo.isConnected();

        } catch (Exception e) {
        	Utils.log2File("CheckConnectivity Exception: " + e.getMessage());
        	Utils.log2File(e);
        	Log.e(Constans.TAG, e.getMessage(), e);
        }
        return false;
    }
}
