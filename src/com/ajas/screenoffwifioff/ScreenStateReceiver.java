package com.ajas.screenoffwifioff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class ScreenStateReceiver extends BroadcastReceiver {
    
  @Override
  public void onReceive(Context context, Intent intent) {
	  if(WifiActivity.development) Utils.log2File("Received action: " + intent.getAction());
	  
	  SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
 	  boolean wifiOffAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_OFF, false);
 	  boolean wifiOnAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_ON, false);
 	  
 	  boolean dataOffAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_OFF, false);
	  boolean dataOnAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_ON, false);
	  
      if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
    	  if(wifiOffAction) {
	    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.WIFI_ON))
	    		 EventsQueue.getInstance().remove(Event.WIFI_ON);
	    	 
	    	 if(WifiActivity.development) Utils.log2File("preferences action wifi "+wifiOffAction);
	    	 if(wifiOffAction) {
	    		 //EventsQueue.getInstance().clear();
	    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF);    		 
	    		 if(null == delay || "0".equals(delay))
	    			 Utils.stopWifi(context);
	    		 else {
	    			 long delayVal = Long.parseLong(delay);
	    			 EventsQueue.getInstance().add(new Event("Wifi off", delayVal * 1000, Event.WIFI_OFF));
	    		 }
	    		 //System.out.println("Wifi off added, delay: "+ delay);
	    	 }
    	  }
    	  
		  if(dataOffAction) {
	    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.DATA_ON))
	    		 EventsQueue.getInstance().remove(Event.DATA_ON);
	    	 
	    	 if(WifiActivity.development) Utils.log2File("preferences action data "+dataOffAction);
	    	 if(dataOffAction) {
	    		 //EventsQueue.getInstance().clear();
	    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_OFF);    		 
	    		 if(null == delay || "0".equals(delay))
	    			 Utils.stopData(context);
	    		 else {
	    			 long delayVal = Long.parseLong(delay);
	    			 EventsQueue.getInstance().add(new Event("Data off", delayVal * 1000, Event.DATA_OFF));
	    		 }
	    		 //System.out.println("Wifi off added, delay: "+ delay);
	    	 } 
		  }
      } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) { 
    	  if(wifiOnAction) {
	    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.WIFI_OFF))
	    		 EventsQueue.getInstance().remove(Event.WIFI_OFF);
	    	 
	    	 //SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
	    	 if(WifiActivity.development) Utils.log2File("preferences action wifi"+wifiOnAction);
	    	 if(wifiOnAction) {
	    		 //EventsQueue.getInstance().clear();
	    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON);
	    		 if(null == delay || "0".equals(delay))
	    			 Utils.startWifi(context);
	    		 else {
	    			 long delayVal = Long.parseLong(delay);
	    			 EventsQueue.getInstance().add(new Event("Wifi on", delayVal * 1000, Event.WIFI_ON));
	    		 }
	    		 //System.out.println("Wifi on added, delay: "+ delay);
	    	 }
    	  }
    	  
		  if(dataOnAction) {
	    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.DATA_OFF))
	    		 EventsQueue.getInstance().remove(Event.DATA_OFF);
	    	 
	    	 //SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
	    	 if(WifiActivity.development) Utils.log2File("preferences action data "+dataOnAction);
	    	 if(dataOnAction) {
	    		 //EventsQueue.getInstance().clear();
	    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_ON);
	    		 if(null == delay || "0".equals(delay))
	    			 Utils.startData(context);
	    		 else {
	    			 long delayVal = Long.parseLong(delay);
	    			 EventsQueue.getInstance().add(new Event("Data on", delayVal * 1000, Event.DATA_ON));
	    		 }
	    		 //System.out.println("Wifi on added, delay: "+ delay);
	    	 }
    	  }
      }
      
      /*Intent i = new Intent(context, ScreenStateService.class);
      i.putExtra("screen_state", screenOff);
      context.startService(i);*/
  }

}
