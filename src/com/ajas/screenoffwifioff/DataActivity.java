package com.ajas.screenoffwifioff;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;

public class DataActivity extends Activity {
	public static int delayOffValue = 0;
	public static int delayOnValue = 0;
	public static int delayOffWifiValue = 0;
	public static int delayOnWifiValue = 0;
	public static int delayNoNetworkOffValue = 0;
	public static int delayLowSignalOffValue = 0;
	public static boolean clearActivity = false;
	
	public static boolean development = false;
	
	SharedPreferences sharedPreferences;
	Button dataOff;
	Button dataOn;
	EditText delayOff;
	EditText delayOn;
	
	Button dataOffWifi;
	Button dataOnWifi;
	EditText delayOffWifi;
	EditText delayOnWifi;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.data);
		
		/*IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenStateReceiver();
        registerReceiver(mReceiver, filter);*/
		
		//Intent i = new Intent(this, ScreenStateService.class);
        //startService(i);
		
		if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
		
		dataOff = (Button) findViewById(R.id.dataOffBtn);
		dataOff.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_OFF, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_OFF, state);
	            updateLabels();
	        }
	    });
		
		dataOn = (Button) findViewById(R.id.dataOnBtn);
		dataOn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_ON, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_ON, state);
	            updateLabels();
	        }
	    });
		
		
		delayOff = (EditText)findViewById(R.id.dataEditText1);
        String delayOffValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_OFF);
        if(StringUtils.isEmpty(delayOffValueStr)) delayOffValueStr = "0";
        /*String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	int position = Utils.getArrayPosition(delayOffValueStr, options);
       	if(position>-1) delayOff.setSelection(position);*/
        delayOff.setText(delayOffValueStr);
       	delayOffValue = Integer.parseInt(delayOffValueStr);
       	delayOff.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) { }
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOffValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_OFF, text);
       	    }

       	});
       	/*delayOff.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
                int index = arg0.getSelectedItemPosition();
               // storing string resources into Array
               String[] options = getResources().getStringArray(R.array.delayTimeValue);
               Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF, options[index]);
               delayOffValue = Integer.parseInt(options[index]);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        });*/
       	
       	delayOn = (EditText)findViewById(R.id.dataEditText2);
        String delayOnValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_ON);
        if(StringUtils.isEmpty(delayOnValueStr)) delayOnValueStr = "0";
       	//String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	/*position = Utils.getArrayPosition(delayOnValueStr, options);
       	if(position>-1) delayOn.setSelection(position);*/
        delayOn.setText(delayOnValueStr);
       	delayOnValue = Integer.parseInt(delayOnValueStr);
       	delayOn.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOnValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_ON, text);
       	    }

       	});
       	/*delayOn.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
                int index = arg0.getSelectedItemPosition();
               // storing string resources into Array
               String[] options = getResources().getStringArray(R.array.delayTimeValue);
               Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON, options[index]);
               delayOnValue = Integer.parseInt(options[index]);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        });*/
       	
       	dataOffWifi = (Button) findViewById(R.id.dataOffWifiBtn);
       	dataOffWifi.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE2, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE2, state);
	            updateLabels();
	        }
	    });
		
		dataOnWifi = (Button) findViewById(R.id.dataOnWifiBtn);
		dataOnWifi.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE, state);
	            updateLabels();
	        }
	    });
       	
       	
       	delayOffWifi = (EditText)findViewById(R.id.editText6);
        String delayOffWifiValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_TOGGLE_OFF);
        if(StringUtils.isEmpty(delayOffWifiValueStr)) delayOffWifiValueStr = "0";
       	//String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	/*position = Utils.getArrayPosition(delayOnValueStr, options);
       	if(position>-1) delayOn.setSelection(position);*/
        delayOffWifi.setText(delayOffWifiValueStr);
       	delayOffWifiValue = Integer.parseInt(delayOffWifiValueStr);
       	delayOffWifi.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOffWifiValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_TOGGLE_OFF, text);
       	    }

       	});
       	
       	delayOnWifi = (EditText)findViewById(R.id.editText5);
        String delayOnWifiValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_TOGGLE_ON);
        if(StringUtils.isEmpty(delayOnWifiValueStr)) delayOnWifiValueStr = "0";
       	//String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	/*position = Utils.getArrayPosition(delayOnValueStr, options);
       	if(position>-1) delayOn.setSelection(position);*/
        delayOnWifi.setText(delayOnWifiValueStr);
       	delayOnWifiValue = Integer.parseInt(delayOnWifiValueStr);
       	delayOnWifi.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOnWifiValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_DATA_TOGGLE_ON, text);
       	    }

       	});
       	
       	updateLabels();
		
       	findViewById(R.id.main_layout).requestFocus();
	}
	
	private void updateLabels() {
		if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(DataActivity.this);
		
        boolean stateOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_OFF, false);
        if(stateOff) dataOff.setText(getResources().getString(R.string.disable));
        else dataOff.setText(getResources().getString(R.string.enable));
        
        boolean stateOn = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_ON, false);
        if(stateOn) dataOn.setText(getResources().getString(R.string.disable));
        else dataOn.setText(getResources().getString(R.string.enable));
        
        boolean stateWifiOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE2, false);
        if(stateWifiOff) dataOffWifi.setText(getResources().getString(R.string.disable));
        else dataOffWifi.setText(getResources().getString(R.string.enable));
        
        boolean stateWifiOn = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE, false);
        if(stateWifiOn) dataOnWifi.setText(getResources().getString(R.string.disable));
        else dataOnWifi.setText(getResources().getString(R.string.enable));
        
        /*boolean stateNoNetworkOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, false);
        if(stateNoNetworkOff) wifiNoNetworkOff.setText(getResources().getString(R.string.disable));
        else wifiNoNetworkOff.setText(getResources().getString(R.string.enable));
        
        boolean stateLowSignalOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, false);
        if(stateLowSignalOff) wifiLowSignalOff.setText(getResources().getString(R.string.disable));
        else wifiLowSignalOff.setText(getResources().getString(R.string.enable));*/
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/
}
