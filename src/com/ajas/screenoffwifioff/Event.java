package com.ajas.screenoffwifioff;

public class Event {
	public static final String WIFI_OFF = "WIFI_OFF";
	public static final String WIFI_OFF_NO_NETWORK = "WIFI_OFF_NO_NETWORK";
	public static final String WIFI_OFF_LOW_SIGNAL = "WIFI_OFF_LOW_SIGNAL";
	public static final String WIFI_ON = "WIFI_ON";
	public static final String DATA_OFF = "DATA_OFF";
	public static final String DATA_ON = "DATA_ON";
	
	private String name;
	private Long time;
	private String kind;
	
	public Event(String name, Long time, String kind) {
		this.name = name;
		this.time = System.currentTimeMillis()+time;
		this.kind = kind;		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public static String getWifiOff() {
		return WIFI_OFF;
	}

	public static String getWifiOn() {
		return WIFI_ON;
	}
}
