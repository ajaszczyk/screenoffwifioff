package com.ajas.screenoffwifioff;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;

public class WifiActivity extends Activity {
	public static int delayOffValue = 0;
	public static int delayOnValue = 0;
	public static int delayNoNetworkOffValue = 0;
	public static int delayLowSignalOffValue = 0;
	public static boolean clearActivity = false;
	
	public static boolean development = false;
	
	SharedPreferences sharedPreferences;
	Button wifiOff;
	Button wifiOn;
	Button wifiNoNetworkOff;
	Button wifiLowSignalOff;
	Button wifiPowerOff;
	Button wifiPowerOn;
	EditText delayOff;
	EditText delayOn;
	EditText delayNoNetworkOff;
	EditText delayLowSignalOff;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wifi);
		
		/*IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenStateReceiver();
        registerReceiver(mReceiver, filter);*/
		
		wifiOff = (Button) findViewById(R.id.dataOffBtn);
		wifiOff.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_OFF, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_OFF, state);
	            updateLabels();
	        }
	    });
		
		wifiOn = (Button) findViewById(R.id.dataOnBtn);
		wifiOn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_ON, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_ON, state);
	            updateLabels();
	        }
	    });
		
		
		wifiNoNetworkOff = (Button) findViewById(R.id.wifiOffBtn2);
       	wifiNoNetworkOff.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, state);
	            updateLabels();
	        }
	    });
       	
       	wifiLowSignalOff = (Button) findViewById(R.id.wifiOffBtn3);
       	wifiLowSignalOff.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, state);
	            updateLabels();
	        }
	    });
       	
       	wifiPowerOff = (Button) findViewById(R.id.wifiOffPwrBtn);
       	wifiPowerOff.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_OFF, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_OFF, state);
	            updateLabels();
	        }
	    });
		
		wifiPowerOn = (Button) findViewById(R.id.wifiOnPwrBtn);
		wifiPowerOn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	        	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
	            boolean state = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_ON, false);
	            Utils.setPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_ON, state);
	            updateLabels();
	        }
	    });
       	
       	if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(WifiActivity.this);
		
		updateLabels();
		
		delayOff = (EditText)findViewById(R.id.dataEditText1);
        String delayOffValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF);
        if(StringUtils.isEmpty(delayOffValueStr)) delayOffValueStr = "0";
        /*String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	int position = Utils.getArrayPosition(delayOffValueStr, options);
       	if(position>-1) delayOff.setSelection(position);*/
        delayOff.setText(delayOffValueStr);
       	delayOffValue = Integer.parseInt(delayOffValueStr);
       	delayOff.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) { }
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOffValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF, text);
       	    }

       	});
       	/*delayOff.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
                int index = arg0.getSelectedItemPosition();
               // storing string resources into Array
               String[] options = getResources().getStringArray(R.array.delayTimeValue);
               Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF, options[index]);
               delayOffValue = Integer.parseInt(options[index]);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        });*/
       	
       	delayOn = (EditText)findViewById(R.id.dataEditText2);
        String delayOnValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON);
        if(StringUtils.isEmpty(delayOnValueStr)) delayOnValueStr = "0";
       	//String[] options = getResources().getStringArray(R.array.delayTimeValue);
       	/*position = Utils.getArrayPosition(delayOnValueStr, options);
       	if(position>-1) delayOn.setSelection(position);*/
        delayOn.setText(delayOnValueStr);
       	delayOnValue = Integer.parseInt(delayOnValueStr);
       	delayOn.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayOnValue = Integer.parseInt(text);
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON, text);
       	    }

       	});
       	/*delayOn.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
                int index = arg0.getSelectedItemPosition();
               // storing string resources into Array
               String[] options = getResources().getStringArray(R.array.delayTimeValue);
               Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON, options[index]);
               delayOnValue = Integer.parseInt(options[index]);
            }

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
        });*/
       	
       	delayNoNetworkOff = (EditText)findViewById(R.id.editText3);
        String delayNoNetworkOffValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_NO_NETWOKR_OFF);
        if(StringUtils.isEmpty(delayNoNetworkOffValueStr)) delayNoNetworkOffValueStr = "0";
        
        delayNoNetworkOffValue = Integer.parseInt(delayNoNetworkOffValueStr);
		if(delayNoNetworkOffValue < 30) {
			delayNoNetworkOffValue = 30;
			delayNoNetworkOff.setText("30");
			Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_NO_NETWOKR_OFF, "30");
		}
        
        delayNoNetworkOff.setText(delayNoNetworkOffValueStr);
       	delayNoNetworkOffValue = Integer.parseInt(delayNoNetworkOffValueStr);
       	delayNoNetworkOff.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayNoNetworkOffValue = Integer.parseInt(text);				
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_NO_NETWOKR_OFF, text);
       	    }
       	});
       	delayNoNetworkOff.setOnFocusChangeListener(new OnFocusChangeListener() {          
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                	String text = delayNoNetworkOff.getText().toString();
    				if(StringUtils.isEmpty(text)) text = "0";
    				delayNoNetworkOffValue = Integer.parseInt(text);
    				if(delayNoNetworkOffValue < 30) {
    					delayNoNetworkOffValue = 30;
    					text = "30";
    				}
    				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_NO_NETWOKR_OFF, text);
    				delayNoNetworkOff.setText(text);
                }
            }
        });
       	
       	delayLowSignalOff = (EditText)findViewById(R.id.editText4);
        String delayLowSignalOffValueStr = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_LOW_SIGNAL_OFF);
        if(StringUtils.isEmpty(delayLowSignalOffValueStr)) delayLowSignalOffValueStr = "0";
        
        delayLowSignalOffValue = Integer.parseInt(delayLowSignalOffValueStr);
		if(delayLowSignalOffValue < 30) {
			delayLowSignalOffValue = 30;
			delayLowSignalOff.setText("30");
			Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_LOW_SIGNAL_OFF, "30");
		}
        
        delayLowSignalOff.setText(delayLowSignalOffValueStr);
       	delayLowSignalOffValue = Integer.parseInt(delayLowSignalOffValueStr);
       	delayLowSignalOff.addTextChangedListener(new TextWatcher() {
       		@Override
			public void afterTextChanged(Editable arg0) {}
       		
       	    @Override
       	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

       	    @Override
       	    public void onTextChanged(CharSequence s, int start, int before, int count) {
       	    	String text = s.toString();
				if(StringUtils.isEmpty(text)) text = "0";
				delayLowSignalOffValue = Integer.parseInt(text);				
				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_LOW_SIGNAL_OFF, text);
       	    }
       	});
       	delayLowSignalOff.setOnFocusChangeListener(new OnFocusChangeListener() {          
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                	String text = delayLowSignalOff.getText().toString();
    				if(StringUtils.isEmpty(text)) text = "0";
    				delayLowSignalOffValue = Integer.parseInt(text);
    				if(delayLowSignalOffValue < 30) {
    					delayLowSignalOffValue = 30;
    					text = "30";
    				}
    				Utils.setPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_LOW_SIGNAL_OFF, text);
    				delayLowSignalOff.setText(text);
                }
            }
        });
       	
		
       	findViewById(R.id.main_layout).requestFocus();
	}
	
	private void updateLabels() {
        boolean stateOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_OFF, false);
        if(stateOff) wifiOff.setText(getResources().getString(R.string.disable));
        else wifiOff.setText(getResources().getString(R.string.enable));
        
        boolean stateOn = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_ON, false);
        if(stateOn) wifiOn.setText(getResources().getString(R.string.disable));
        else wifiOn.setText(getResources().getString(R.string.enable));
        
        boolean stateNoNetworkOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, false);
        if(stateNoNetworkOff) wifiNoNetworkOff.setText(getResources().getString(R.string.disable));
        else wifiNoNetworkOff.setText(getResources().getString(R.string.enable));
        
        boolean stateLowSignalOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, false);
        if(stateLowSignalOff) wifiLowSignalOff.setText(getResources().getString(R.string.disable));
        else wifiLowSignalOff.setText(getResources().getString(R.string.enable));
        
        boolean statePowerOff = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_OFF, false);
        if(statePowerOff) wifiPowerOff.setText(getResources().getString(R.string.disable));
        else wifiPowerOff.setText(getResources().getString(R.string.enable));
        
        boolean statePowerOn = !Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_POWER_ON, false);
        if(statePowerOn) wifiPowerOn.setText(getResources().getString(R.string.disable));
        else wifiPowerOn.setText(getResources().getString(R.string.enable));
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/
}
