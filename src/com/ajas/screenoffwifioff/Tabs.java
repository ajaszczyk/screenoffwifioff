package com.ajas.screenoffwifioff;

import android.app.TabActivity;
import android.content.Context;
import android.os.Build;

public abstract class Tabs {
	public abstract void createTabs(TabActivity activity, Context context);
	
	public static Tabs getInstance() {
    	return new TabsImpl();
	}
}
