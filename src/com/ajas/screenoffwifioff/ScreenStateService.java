package com.ajas.screenoffwifioff;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;

public class ScreenStateService extends Service {
	private BroadcastReceiver mReceiver;
	private BroadcastReceiver dataReceiver;
	private BroadcastReceiver wifiReceiver;
	private static Timer timer; 
    private Context ctx;
    private SharedPreferences sharedPreferences;
    
    private int heartBeat;
    private int heartBeatSeconds;
    
    private static ScreenStateService _instance = null;
    
	 @Override
     public void onCreate() {
         super.onCreate();
         
         _instance = this;
         
         heartBeat = 0;
         heartBeatSeconds = 60;
         
         if(null == timer) timer = new Timer();
         ctx = this;
         
         if(null == sharedPreferences) sharedPreferences = Utils.getSharedPreferences(ctx);
         
         if(Utils.getPreferenceBooleanValue(Utils.getDefaultSharedPreferences(this), Constans.PREFERENCES_KEEP_FOREGROUND, true)) {
        	 startFg();
         }
         
         if(WifiActivity.development) Utils.log2File("Service registered");
     }

	 @Override
	 public void onDestroy() {
		super.onDestroy();
	 	
		if(null != mReceiver) unregisterReceiver(mReceiver);
		if(null != dataReceiver) unregisterReceiver(dataReceiver);
		if(null != wifiReceiver) unregisterReceiver(wifiReceiver);
		
		if(Utils.getPreferenceBooleanValue(Utils.getDefaultSharedPreferences(this), Constans.PREFERENCES_KEEP_FOREGROUND, true)) {
			stopFg();
		}
		
	 	if(WifiActivity.development) Utils.log2File("service onDestroy");
	 }
	 
	 public static ScreenStateService getInstance() {
		 return _instance;
	 }
	 
	 public void stopFg() {
		 stopForeground(true);
	 }
	 
	 public void startFg() {
		 Notification note = Utils.createForegroundNotification(this, R.drawable.wifi_off, "", null, MainActivity.APP_ID.intValue());
 		 startForeground(MainActivity.APP_ID, note);
	 }
	 
     /*@Override
     public void onStart(Intent intent, int startId)  {*/
	 
	 @Override
     public int onStartCommand(Intent intent, int flags, int startId) {
		 // register receiver that handles screen on and screen off logic
		 IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
         filter.addAction(Intent.ACTION_SCREEN_OFF);
         mReceiver = new ScreenStateReceiver();
         registerReceiver(mReceiver, filter);
		 
         /*IntentFilter dataFilter = new IntentFilter(TelephonyIntents.ACTION_SERVICE_STATE_CHANGED);
         //dataFilter.addAction(TelephonyManager.DATA_CONNECTED);
         dataReceiver = new MobileDataReceiver();
         registerReceiver(dataReceiver, dataFilter);*/
         
         /*IntentFilter wifiFilter = new IntentFilter();
         wifiFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
         wifiFilter.addAction(WifiManager.SUPPLICANT_CONNECTION_CHANGE_ACTION);
         wifiFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
         wifiFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
         registerReceiver(wifiReceiver, wifiFilter);*/
         
    	 timer.scheduleAtFixedRate(new timerTask(), 1000, 1000); 
    	 
    	 if(WifiActivity.development) Utils.log2File("Service started");
    	 
        /* boolean screenOn = intent.getBooleanExtra("screen_state", false);
         
         if (!screenOn) {
       	  SharedPreferences sharedPreferences = Utils.getSharedPreferences(this);
       	  boolean action = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_OFF, false);
       	  if(action) {
       		  WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
       		  boolean wifiEnabled = wifiManager.isWifiEnabled();
       		  if(wifiEnabled) wifiManager.setWifiEnabled(false);
       	  }	
         } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
       	  SharedPreferences sharedPreferences = Utils.getSharedPreferences(this);
       	  boolean action = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_ON, false);
       	  if(action) {
       		  WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
       		  boolean wifiEnabled = wifiManager.isWifiEnabled();
       		  if(!wifiEnabled) wifiManager.setWifiEnabled(true);
       	  }
         }*/
         
         /*if (!screenOn) {
             // your code
         } else {
             // your code
         }*/
         
         return START_STICKY;
     }

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private class timerTask extends TimerTask { 
        public void run() {
        	if(WifiActivity.development){
	        	heartBeat++;
	        	if(heartBeat >= heartBeatSeconds ) {
	        		heartBeat = 0;
	        		heartBeatSeconds = 3600;
	        		Utils.log2File("Heart Beat - I'm still alive...");
	        	}
        	}
            Event event = EventsQueue.getInstance().get();
            if(null != event) {
            	if(WifiActivity.development) Utils.log2File("timerTask: "+event.getName());
            	if(Event.WIFI_OFF.equals(event.getKind())) Utils.stopWifi(ctx);
            	else if(Event.WIFI_ON.equals(event.getKind())) Utils.startWifi(ctx);
            	else if(Event.WIFI_OFF_NO_NETWORK.equals(event.getKind())){            		
            		if(!Utils.isWifiConnected(ctx)) Utils.stopWifi(ctx);
            	} else if(Event.WIFI_OFF_LOW_SIGNAL.equals(event.getKind())){
            		checkWifiStrength();
            	}
            	else if(Event.DATA_OFF.equals(event.getKind())) Utils.stopData(ctx);
            	else if(Event.DATA_ON.equals(event.getKind())) Utils.startData(ctx);
            }
            
            checkWifi();
            checkWifiStrength();
        }
        
        private void checkWifi() {        	
        	boolean action = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, false);
         	if(action) {
	    		boolean wifiEnabled = Utils.isWifiEnabled(ctx);
	    		if(wifiEnabled) {
	    			boolean wifiConnected = Utils.isWifiConnected(ctx);
	    			if(!wifiConnected) {
		    			String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_NO_NETWOKR_OFF);    		 
		    	    	if(null == delay) delay = "0";
		    	    	long delayVal = Long.parseLong(delay);
		    			boolean result = EventsQueue.getInstance().add(new Event("Wifi off no network", delayVal * 1000, Event.WIFI_OFF_NO_NETWORK));
		    			if(result || WifiActivity.development) Utils.log2File("Wifi off no network added, delay: "+ delay);
	    			}
	    		}
         	}
        }
        
        private void checkWifiStrength() {        	
        	boolean action = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, false);
         	if(action) {
	    		boolean wifiEnabled = Utils.isWifiEnabled(ctx);
	    		if(wifiEnabled) {
	    			boolean wifiConnected = Utils.isWifiConnected(ctx);
	    			if(!wifiConnected) {
	    				int signalStrength = Utils.getWifiSignalStrength(ctx);
	    				if(signalStrength < 20) {
	    					String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_LOW_SIGNAL_OFF);    		 
			    	    	if(null == delay) delay = "0";
			    	    	long delayVal = Long.parseLong(delay);
			    			boolean result = EventsQueue.getInstance().add(new Event("Wifi off low signal", delayVal * 1000, Event.WIFI_OFF_LOW_SIGNAL));
			    			if(result || WifiActivity.development) Utils.log2File("Wifi off low signal added, delay: "+ delay);
	    				} else if(EventsQueue.getInstance().hasEventOfSameType(Event.WIFI_OFF_LOW_SIGNAL)) 
	    					EventsQueue.getInstance().clear();
	    				//System.out.println("Wifi signal strength: "+signalStrength);
	    				//Utils.showToast("Wifi signal strength: "+signalStrength, false, ctx);
	    				if(WifiActivity.development) Utils.log2File("Wifi signal strength: "+signalStrength);
		    			
	    			}
	    		}
         	}
        }
    }
}
