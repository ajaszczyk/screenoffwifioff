package com.ajas.screenoffwifioff;

public class StringUtils {
	public static boolean isEmpty(String string){
		return (null == string || string.length() == 0);
	}
	
	public static boolean isNotEmpty(String string){
		return (null != string && string.length() > 0);
	}
	
	public static int getLength(String string){
		if(null != string) return string.trim().length();
		else return 0;
	}
}
