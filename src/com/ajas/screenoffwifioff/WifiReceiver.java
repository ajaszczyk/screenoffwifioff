package com.ajas.screenoffwifioff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class WifiReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(WifiActivity.development) Utils.log2File("Received action: " + intent.getAction());
		
		SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
		boolean dataToogleAction = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE, false);
		boolean dataToogleAction2 = Utils.getPreferenceBooleanValue(sharedPreferences, Constans.PREFERENCES_DATA_TOOGLE2, false);
		
		if(Utils.isWifiConnected(context)) {
			if(dataToogleAction) {
		    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.DATA_ON))
		    		 EventsQueue.getInstance().remove(Event.DATA_ON);
		    	 Utils.stopData(context);
		    	 
		    	 /*if(WifiActivity.development) Utils.log2File("preferences action data "+dataToogleAction);
		    	 if(dataToogleAction) {
		    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_OFF);    		 
		    		 if(null == delay || "0".equals(delay))
		    			 Utils.stopWifi(context);
		    		 else {
		    			 long delayVal = Long.parseLong(delay);
		    			 EventsQueue.getInstance().add(new Event("Wifi off", delayVal * 1000, Event.WIFI_OFF));
		    		 }
		    		 //System.out.println("Wifi off added, delay: "+ delay);
		    	 }*/
	    	  }
		} else {
			if(dataToogleAction2) {
		    	 if(EventsQueue.getInstance().hasEventOfSameType(Event.DATA_OFF))
		    		 EventsQueue.getInstance().remove(Event.DATA_OFF);
		    	 Utils.startData(context);
		    	 
		    	 /*if(WifiActivity.development) Utils.log2File("preferences action data"+dataToogleAction);
		    	 if(dataToogleAction) {
		    		 String delay = Utils.getPreferenceValue(sharedPreferences, Constans.PREFERENCES_DELAY_ON);
		    		 if(null == delay || "0".equals(delay))
		    			 Utils.startWifi(context);
		    		 else {
		    			 long delayVal = Long.parseLong(delay);
		    			 EventsQueue.getInstance().add(new Event("Wifi on", delayVal * 1000, Event.WIFI_ON));
		    		 }
		    		 //System.out.println("Wifi on added, delay: "+ delay);
		    	 }*/
	    	  }
		}
	}
}
