package com.ajas.screenoffwifioff;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class BootReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences sharedPreferences = Utils.getSharedPreferences(context);
		boolean actionOff = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_OFF, false);
		boolean actionOn = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_ON, false);
		boolean actionNoNetworkOff = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_NO_NETWORK_OFF, false);
		boolean actionLowSignalOff = sharedPreferences.getBoolean(Constans.PREFERENCES_WIFI_LOW_SIGNAL_OFF, false);
		
		if(WifiActivity.development) Utils.log2File("Boot off " + actionOff + " on " + actionOn + " no_net " + actionNoNetworkOff + " low_sig " + actionLowSignalOff);
		
		if(actionOff || actionOn || actionNoNetworkOff || actionLowSignalOff) {
			
			Intent i = new Intent(context, ScreenStateService.class);
			context.startService(i);
			
			/*IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
	        filter.addAction(Intent.ACTION_SCREEN_OFF);
	        BroadcastReceiver mReceiver = new ScreenStateReceiver();
	        context.registerReceiver(mReceiver, filter);*/
		}
	}

}
