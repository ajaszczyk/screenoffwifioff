package com.ajas.screenoffwifioff;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TabHost;

public class MainActivity extends TabActivity {
	FrameLayout mFrameLayout;
	
	TabHost tHost;
	
	public static boolean development = false;
	
	public static boolean boot = false;
	
	SharedPreferences defaultSharedPreferences;
	
	public static final Integer APP_ID = 123475689;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab);
        
        defaultSharedPreferences = Utils.getDefaultSharedPreferences(this);
	    
        Tabs tabs = Tabs.getInstance();
        tabs.createTabs(this, this.getBaseContext());
        
        tHost = (TabHost) findViewById(android.R.id.tabhost);
        tHost.setup();
        
        Intent i = new Intent(this, ScreenStateService.class);
        startService(i);
        
        SharedPreferences sharedPreferences = Utils.getSharedPreferences(this);
        
        Utils.log2File(Utils.getDeviceInfo());
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	menu.add(Menu.NONE, 0, 0, getResources().getString(R.string.preferencesShow));
        menu.add(Menu.NONE, 1, 0, getResources().getString(R.string.exit));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                startActivity(new Intent(this, PrefsActivity.class));
                return true;
            case 1:
            	if(null != ScreenStateService.getInstance())
					ScreenStateService.getInstance().stopFg();
                this.moveTaskToBack(true);
                break;
        }
        return false;
    }
    
}