package com.ajas.screenoffwifioff;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

public class TabsImpl extends Tabs{
	private TabActivity activity;
	private Context context;
	
	public static final String WIFI = "WIFI";
	public static final String DATA = "DATA";
	
	@Override
	public void createTabs(TabActivity activity, Context context) {
		this.activity = activity;
		this.context = context;
		
		Intent backupIntent = new Intent(context, WifiActivity.class);
        backupIntent.putExtra(WIFI, true);
        addTab(WIFI, context.getResources().getString(R.string.wifi), R.drawable.tab_one, backupIntent);
        
        Intent restoreIntent = new Intent(context, DataActivity.class);
        addTab(DATA, context.getResources().getString(R.string.data), R.drawable.tab_two, restoreIntent);
	}

	private void addTab(String tabSpec, String label, int drawableId, Intent intent) {
		TabHost.TabSpec spec = activity.getTabHost().newTabSpec(tabSpec);

		View tabIndicator = null;
		/*if(Utils.isRightToLeft()) tabIndicator = LayoutInflater.from(context).inflate(R.layout.tab_indicator_rtl, activity.getTabWidget(), false);
		else */tabIndicator = LayoutInflater.from(context).inflate(R.layout.tab_indicator, activity.getTabWidget(), false);
		TextView title = (TextView) tabIndicator.findViewById(R.id.title);
		title.setText(label);
		ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
		icon.setImageResource(drawableId);

		spec.setIndicator(tabIndicator);
		spec.setContent(intent);
		activity.getTabHost().addTab(spec);
		
		activity.getTabHost().setOnTabChangedListener(new OnTabChangeListener(){
			@Override
			public void onTabChanged(String tabId) {
			    /*if(WIFI.equals(tabId)) {
			    	WifiActivity.getMyContext().setBackup(true);
			    }
			    if(Constans.TAB_RESTORE.equals(tabId)) {
			    	WifiActivity.getMyContext().setBackup(false);
			    }*/
			}});
	}
}
