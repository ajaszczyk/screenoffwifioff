package com.ajas.screenoffwifioff;

public class Constans {
	public static final String TAG = "ScreenOffWifiOff";
	public static final String WORKING_DIRECTORY = "ScreenOffWifiOff";
	public static final String WORKING_DIRECTORY_KITKAT = "Android/data/com.ajas.screenoffwifioff";
	public static final String LOG_FILE = "log.txt";
	public static final String PREFERENCES_FILE_NAME = "ScreenOffWifiOffPreferences";
	public static final String PREFERENCES_WIFI_OFF = "preferences_wifi_off";
	public static final String PREFERENCES_WIFI_ON = "preferences_wifi_on";
	public static final String PREFERENCES_WIFI_NO_NETWORK_OFF = "preferences_wifi_no_network_off";
	public static final String PREFERENCES_WIFI_LOW_SIGNAL_OFF = "preferences_wifi_low_signal_off";
	public static final String PREFERENCES_WIFI_POWER_OFF = "preferences_wifi_power_off";
	public static final String PREFERENCES_WIFI_POWER_ON = "preferences_wifi_power_on";
	public static final String PREFERENCES_DELAY_OFF = "preferences_delay_off";
	public static final String PREFERENCES_DELAY_ON = "preferences_delay_on";
	public static final String PREFERENCES_DELAY_NO_NETWOKR_OFF = "preferences_delay_no_network_off";
	public static final String PREFERENCES_DELAY_LOW_SIGNAL_OFF = "preferences_delay_low_signal_off";
	public static final String PREFERENCES_DATA_OFF = "preferences_data_off";
	public static final String PREFERENCES_DATA_ON = "preferences_data_on";
	public static final String PREFERENCES_DELAY_DATA_OFF = "preferences_delay_data_off";
	public static final String PREFERENCES_DELAY_DATA_ON = "preferences_delay_data_on";
	public static final String PREFERENCES_WIFI_TOOGLE = "preferences_wifi_toogle";
	public static final String PREFERENCES_DATA_TOOGLE = "preferences_data_toogle";
	public static final String PREFERENCES_DATA_TOOGLE2 = "preferences_data_toogle2";
	public static final String PREFERENCES_DELAY_DATA_TOGGLE_OFF = "preferences_delay_data_toogle_off";
	public static final String PREFERENCES_DELAY_DATA_TOGGLE_ON = "preferences_delay_data_toogle_on";
	public static final String PREFERENCES_ADS_FUL_SKRIN_NEW = "ads_ful_skrin_new";
	public static final String PREFERENCES_KEEP_FOREGROUND = "keep_foreground";
	public static final String PREFERENCES_WIFI_POWER_OFF_EXCEPT = "wifi_power_off_except";
}
